const hamburger = document.getElementById('hamburger');
const hOverlay = document.getElementById('h-overlay');
const cross = document.getElementById('cross')


hamburger.addEventListener('click', () =>{
    hOverlay.style.display = 'flex';
    cross.style.display = 'flex';
    document.body.style.overflow = 'hidden';
})


cross.addEventListener('click', () =>{
    hOverlay.style.display = 'none';
    cross.style.display = 'none';
    document.body.style.overflow = 'scroll';
})

hOverlay.addEventListener('click', () =>{
    hOverlay.style.display = 'none';
    cross.style.display = 'none';
    document.body.style.overflow = 'scroll';
})