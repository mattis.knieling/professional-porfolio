let skillsButton = document.querySelector('#skills-button');

console.log(skillsButton)
const htmlSkills = document.getElementById('html-skills');
const cssSkills = document.getElementById('css-skills');
const jsSkills = document.getElementById('js-skills');
const vueSkills = document.getElementById('vue-skills');
const pySkills = document.getElementById('py-skills');
const csharpSkills = document.getElementById('csharp-skills');
const gitSkills = document.getElementById('git-skills');
const wpSkills = document.getElementById('wp-skills');
const ubuntuSkills = document.getElementById('ubuntu-skills');
const photoshopSkills = document.getElementById('photoshop-skills');
const msoSkills = document.getElementById('mso-skills');
const sqlSkills = document.getElementById('sql-skills');


skillsButton.addEventListener('click', function(){

  htmlSkills.style.width = '90%' ;
  cssSkills.style.width = '90%' ;
  jsSkills.style.width = '30%' ;
  vueSkills.style.width = '30%' ;
  pySkills.style.width = '25%' ;
  csharpSkills.style.width = '15%' ;
  gitSkills.style.width = '60%' ;
  wpSkills.style.width = '50%' ;
  ubuntuSkills.style.width = '65%' ;
  photoshopSkills.style.width = '55%' ;
  msoSkills.style.width = '80%' ;
  sqlSkills.style.width = '40%' ;


});